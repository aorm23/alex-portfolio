## Synopsis

Online portfolio for Web Developer Alex Ormrod.
This project uses C#, .NET framework (MVC5), HTML5, CSS3, SASS, Javascript, jQuery, Typescript & Gulp.
