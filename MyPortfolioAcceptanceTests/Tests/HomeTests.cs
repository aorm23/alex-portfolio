﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyPortfolioAcceptanceTests.Pages;

namespace MyPortfolioAcceptanceTests.Tests
{
    [TestClass]
    public class HomeTests
    {

        [TestMethod]
        public void CheckMenuToggleWorks()
        {
            using (var homePage = new HomePage())
            {
                // Select toggle 
                homePage.SelectMenuToggle();

                // Check menu is visible
                Assert.IsTrue(homePage.IsMenuVisible());

                // Select toggle again to close
                homePage.SelectMenuToggle();

                // Check menu not visible
                Assert.IsFalse(homePage.IsMenuVisible());
            }
        }

        [TestMethod]
        public void CheckMenuNavigationWorks()
        {
            using (var homePage = new HomePage())
            {
                // About Me
                homePage.SelectMenuToggle();
                homePage.SelectLink("About Me");
                Assert.IsTrue(homePage.IsElementScrolledIntoView("about"));

                // Work
                homePage.SelectMenuToggle();
                homePage.SelectLink("My Work");
                Assert.IsTrue(homePage.IsElementScrolledIntoView("work"));
            }
        }
    }
}
