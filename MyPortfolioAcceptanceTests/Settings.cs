﻿using System;
using MyPortfolioAcceptanceTests.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace MyPortfolioAcceptanceTests
{
    public static class Settings
    {

        private static XDocument XDoc;

        public static string BaseUrl => GetParamVal("BaseUrl");

        public static TargetPlatform TargetPlatform => GetParamVal("TargetPlatform") == "desktop" ? TargetPlatform.Desktop : TargetPlatform.Mobile;


        static Settings()
        {
            var asm = Assembly.GetExecutingAssembly();
            var path = asm.GetManifestResourceStream("MyPortfolioAcceptanceTests.settings.xml");
            XDoc = XDocument.Load(path);
        }


        private static string GetParamVal(string paramName)
        {
            var value = XDoc.Descendants(paramName).First().Attribute("value").Value.ToString();
            return value;
        }
    }
}
