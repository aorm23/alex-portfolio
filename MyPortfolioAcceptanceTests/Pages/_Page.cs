﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyPortfolioAcceptanceTests.Entities;
using MyPortfolioAcceptanceTests.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.PageObjects;

namespace MyPortfolioAcceptanceTests.Pages
{
    public class Page : IDisposable
    {
        public const int AnimationDelaysMs = 1800;

        protected IWebDriver Driver { get; set; }

        [FindsBy(How = How.ClassName, Using = "js-menu-toggle")]
        private IWebElement menuToggle;

        [FindsBy(How = How.TagName, Using = "menu")]
        private IWebElement menu;


        public Page()
        {
            var options = new ChromeOptions();

            if (Settings.TargetPlatform == TargetPlatform.Mobile)
            {
                options.EnableMobileEmulation("Apple iPhone 6");
            }

            this.Driver = new ChromeDriver("C:\\chromedriver", options);

            this.GoToBaseUrl();

            PageFactory.InitElements(this.Driver, this);
        }

        public void GoToBaseUrl()
        {
            this.Driver.Navigate().GoToUrl(Settings.BaseUrl);
        }


        protected void WaitForAnimationsToComplete()
        {
            Thread.Sleep(AnimationDelaysMs);
        }

        public void SelectMenuToggle()
        {
            this.WaitForAnimationsToComplete();
            this.menuToggle.SafeClick();
        }

        public bool IsMenuVisible()
        {
            this.WaitForAnimationsToComplete();
            return this.menu.Displayed;
        }

        public double GetElementPositionFromTop(string elementId)
        {
            object offset = this.Driver.ExecuteJavaScript<object>
                ($"var offset = document.getElementById('{elementId}').getBoundingClientRect(); " +
                 $"return offset.top;");

            return double.Parse($"{offset}");
        }

        public bool IsElementScrolledIntoView(string elementId)
        {
            this.WaitForAnimationsToComplete();
            var positionFromTop = GetElementPositionFromTop(elementId);
            return positionFromTop < 1;
        }

        public void SelectLink(string text)
        {
            this.WaitForAnimationsToComplete();
            var link = this.Driver.FindElements(By.TagName("a")).FirstOrDefault(a => a.Text.ToLower().Equals(text.ToLower()));
            link.Click();
        }

        public void Dispose()
        {
            this.Driver.Quit();
        }
    }
}
