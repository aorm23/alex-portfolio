﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace MyPortfolioAcceptanceTests.Pages
{
    class HomePage : Page
    {

        [FindsBy(How = How.ClassName, Using = "js-menu-toggle")]
        private IWebElement menuToggle;

        [FindsBy(How = How.TagName, Using = "menu")]
        private IWebElement menu;
    }
}
