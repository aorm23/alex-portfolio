﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace MyPortfolioAcceptanceTests.Extensions
{
    public static class WebElementExtensions
    {

        public static void SafeClick(this IWebElement element)
        {
            int attempts = 0;
            int allowedAttempts = 5;

            while (attempts < allowedAttempts)
            {
                attempts++;

                try
                {
                    element.Click();
                    Console.WriteLine($"Click attempt #{attempts}");
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);

                    if (attempts == allowedAttempts)
                    {
                        Console.WriteLine("Reached max number of click attempts");
                        throw;
                    }

                    continue;
                }

                break;
            }
        }
    }
}
