﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyPortfolio.Controllers;

namespace MyPortfolioUnitTests
{
    [TestClass]
    public class ControllerTests
    {
        [TestMethod]
        public void HomeControllerIndex()
        {
            var controller = new HomeController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
