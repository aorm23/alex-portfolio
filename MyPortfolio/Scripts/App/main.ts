﻿/// <reference path="../typings/jquery/jquery.d.ts" />
const toggleSelector: string = ".js-toggle";
const heroSelector: string = ".js-hero";
const menuSelector: string = "menu";
const menuLinksSelector: string = ".js-menu-links a";

class Events {

    public static menuToggle() {
    
        if ($(menuSelector).hasClass(("is-open"))) {
            Events.hideMenu();
        } else {
            Events.openMenu();
        }
    }

    public static navigateToAnchorLink() {
        setTimeout(() => {
            Events.hideMenu();
        }, 200);
    }

    public static hideMenu() {
        let toggle: JQuery = $(toggleSelector);
        toggle.removeClass("is-active");
        $(menuSelector).addClass("is-closed").removeClass("is-open");
    }

    public static openMenu() {
        let toggle: JQuery = $(toggleSelector);
        toggle.addClass("is-active");
        $(menuSelector).addClass("is-open").removeClass("is-closed");
    }


    public static init() {

        $("#scene").parallax({
            calibrateX: true,
            calibrateY: true,
            invertX: false,
            invertY: true,
            limitX: false,
            limitY: false,
            scalarX: 4,
            scalarY: 4,
            frictionX: 0.2,
            frictionY: 0.8,
            originX: 0.0,
            originY: 1.0
        });

        $("a").smoothScroll({
            speed: 600
        });
    }

    public static load() {
        $("body").addClass("is-loaded");
    }
}

$(document).on("click", menuLinksSelector, Events.navigateToAnchorLink);
$(document).on("click", ".js-menu-toggle", Events.menuToggle);
$(document).on("ready", Events.init);
$(window).on("load", Events.load);

// Tricks touch devices to open tooltips on touch
$(document).on("touchstart", "body", () => { });
