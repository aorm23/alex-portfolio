/// <reference path="../typings/jquery/jquery.d.ts" />
var toggleSelector = ".js-toggle";
var heroSelector = ".js-hero";
var menuSelector = "menu";
var menuLinksSelector = ".js-menu-links a";
var Events = (function () {
    function Events() {
    }
    Events.menuToggle = function () {
        if ($(menuSelector).hasClass(("is-open"))) {
            Events.hideMenu();
        }
        else {
            Events.openMenu();
        }
    };
    Events.navigateToAnchorLink = function () {
        setTimeout(function () {
            Events.hideMenu();
        }, 200);
    };
    Events.hideMenu = function () {
        var toggle = $(toggleSelector);
        toggle.removeClass("is-active");
        $(menuSelector).addClass("is-closed").removeClass("is-open");
    };
    Events.openMenu = function () {
        var toggle = $(toggleSelector);
        toggle.addClass("is-active");
        $(menuSelector).addClass("is-open").removeClass("is-closed");
    };
    Events.init = function () {
        $("#scene").parallax({
            calibrateX: true,
            calibrateY: true,
            invertX: false,
            invertY: true,
            limitX: false,
            limitY: false,
            scalarX: 4,
            scalarY: 4,
            frictionX: 0.2,
            frictionY: 0.8,
            originX: 0.0,
            originY: 1.0
        });
        $("a").smoothScroll({
            speed: 600
        });
    };
    Events.load = function () {
        $("body").addClass("is-loaded");
    };
    return Events;
}());
$(document).on("click", menuLinksSelector, Events.navigateToAnchorLink);
$(document).on("click", ".js-menu-toggle", Events.menuToggle);
$(document).on("ready", Events.init);
$(window).on("load", Events.load);
// Tricks touch devices to open tooltips on touch
$(document).on("touchstart", "body", function () { });
//# sourceMappingURL=main.js.map