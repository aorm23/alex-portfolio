﻿/// <binding ProjectOpened='watch' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var jsFiles = ["Scripts/App/*.js", "Scripts/App/**/*.js"];
var jsMinDir = "Scripts/minified_scripts/";
var jsMinFile = "site.min.js";

var cssFiles = ["Content/css/App/**/*.scss", "Content/css/App/*.scss"];
var cssMinDir = "Content/minified_css/";
var cssMinFile = "site.min.css";

var gulp = require('gulp');

var concat = require('gulp-concat');

// css
var minifyCss = require('gulp-clean-css');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var gcmq = require('gulp-group-css-media-queries');


// js
var uglify = require('gulp-uglify');


gulp.task('js', function () {

    gulp.src(jsFiles)
        .pipe(concat(jsMinFile))
        .pipe(uglify())
        .pipe(gulp.dest(jsMinDir));
});


gulp.task('css', function () {

    gulp.src(cssFiles)
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
        }))
        .pipe(gcmq())
        .pipe(minifyCss())
        .pipe(concat(cssMinFile))
        .pipe(gulp.dest(cssMinDir));
});

gulp.task('watch', function () {
    gulp.watch(cssFiles, ['css']);
    gulp.watch(jsFiles, ['js']);
});