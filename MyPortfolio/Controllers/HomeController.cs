﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyPortfolio.Controllers
{
    public class HomeController : Controller
    {

        #if !DEBUG
        [OutputCache(Duration = 3600)]
        #endif
        public ActionResult Index()
        {
            return View();
        }
    }
}