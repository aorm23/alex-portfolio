﻿using System.Web;
using System.Web.Optimization;

namespace MyPortfolio
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new Bundle("~/bundles/appjs").Include(
                "~/Scripts/minified_scripts/site.min.js"));
            
            bundles.Add(new Bundle("~/bundles/appcss").Include(
                "~/Content/minified_css/site.min.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/vendor/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/vendor/jquery-{version}.js",
                "~/Scripts/vendor/smooth-scroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/vendor/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryparallax").Include(
                "~/Scripts/vendor/jquery.parallax.js"));
        }
    }
}
